//  Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Кожен об'єкт має внутрішнє посилання на інший об'єкт - його прототип. Коли властивість запитується з об'єкту
// і якщо вона не знайдена в самому об'єкті, JavaScript продовжить пошук в його прототипі, і так далі, до тих пір,
// поки властивість не буде знайдена або ж буде досягнемо кінця ланцюжка прототипів.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Виклик super() у конструкторі класу-нащадка використовується для виклику конструктора класу-батька. Це необхідно, коли клас-нащадок розширює функціональність класу-батька і, крім власних властивостей та методів, також має виконати специфічну логіку, що відбувається у класі-батька.






// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get getName() {
    return this.name;
  }
  set setName(newName) {
    this.name = newName;
  }
  get getAge() {
    return this.age;
  }
  set setAge(newAge) {
    this.age = newAge;
  }
  get getSalary() {
    return this.salary;
  }
  set setSalary(newSalary) {
    this.salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get getSalary() {
    return this.salary * 3;
  }
}

const prog1 = new Programmer("Alex", 35, 1300, ["JavaScript", "Java"]);
const prog2 = new Programmer("Maria", 27, 700, ["PHP", "Phyton"]);

console.log(prog1);
console.log(prog2);
